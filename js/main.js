
function onMessage(evt){
	try{

	}catch(Ex){
		console.log(Ex);
	}
}

var gender_selection, age, length, weight, hair_color, eye_color, education, smoking, alcohol, sport, CsvData, count, lang, current_dna_view, current_dna_img, video_name;

jQuery(document).ready(function(){
	lang = 'et';
	// -- Load data
	//loadResults();
	jQuery(".home_btn").css("visibility", "hidden");
	jQuery(".info_btn").css("visibility", "hidden");
	// -- Get EST translations
	getTranslations('et');

	// -- Languages click function
	jQuery('.languages div').click(function(){
		lang = jQuery(this).attr('data-language');
		console.log(lang);
		if(lang){
			getTranslations(lang);
		}
	});

	// -- Row button click
	jQuery('.row_nupud, .info_btn, .close_btn').click(function(){
		if (jQuery(this).hasClass('info_btn') == true){
			current_dna_img = "cell";
			current_dna_view = "2";
			jQuery('.row_nupud').removeClass("dna_focus");
			jQuery('.row_nupud:nth-child(2)').addClass("dna_focus");
		}
		else if(jQuery(this).hasClass('close_btn') == true){
			current_dna_img = "empty";
			current_dna_view = "1";
			jQuery('.row_nupud').removeClass("dna_focus");
			jQuery('.row_nupud:nth-child(1)').addClass("dna_focus");
		}

		else{
			current_dna_view = jQuery(this).attr('dna_view');
			current_dna_img = jQuery(this).attr('data-image');
			jQuery('.row_nupud').removeClass("dna_focus");
			jQuery(this).addClass("dna_focus");
		}
		jQuery(".small_dna_pic").attr("src","images/" + current_dna_img + "_small.png");

		if (current_dna_view != "1"){
			jQuery(".info_btn").css("visibility", "hidden");
			jQuery(".dna_image").css("display", "none");
			jQuery(".dna_text_container").css("display", "");
			jQuery(".dna_text_container > h2").attr("data-translation", current_dna_img + "_title");
			jQuery(".dna_text_container > p").attr("data-translation", current_dna_img + "_text");
			jQuery(".close_btn").css("visibility", "");
			getTranslations(lang);
		}
		else{
			jQuery(".dna_image").css("display", "");
			jQuery(".dna_text_container").css("display", "none");
			jQuery(".info_btn").css("visibility", "");
			jQuery(".close_btn").css("visibility", "hidden");
		}
	});

	jQuery('button[data-button="video"]').click(function(){
		video_name = jQuery(this).attr('data-src');

		jQuery("#playBtn").fadeOut(300);
		var video = document.getElementById("video");
		var seekBar = jQuery('#custom-seekbar');
		seekerTimeout = setTimeout(function(){
			seekBar.fadeOut(1500);
		}, 3000);
		video.src = video_name;
		video.load();
		video.play();
	});


	var vid = document.getElementById("video");
	var progressCircle = document.querySelector('.progress-circle-prog');
	var playBtn = jQuery("#playBtn");
	var seekBar = jQuery("#custom-seekbar");

	vid.ontimeupdate = function(){
		var percentage = ( vid.currentTime / vid.duration ) * 100;
		var change = 'calc('+percentage+'% - 13px)';
		$("#seeker-line span").css("left", change);
		progressRadius = (180 * percentage) / 100
		progressCircle.style.strokeDasharray = (progressRadius) + ' 999';
	};

	vid.addEventListener('click',function(){
		if (this.paused) {
			playBtn.fadeOut(300);
			seekerTimeout = setTimeout(function(){
				seekBar.fadeOut(1500);
			}, 3000);
			this.play();
		} else {
			clearTimeout(seekerTimeout);
			playBtn.fadeIn(300);
			seekBar.fadeIn(300);
			this.pause();
		}
	},false);

	vid.addEventListener('ended',function(){
		playBtn.fadeIn(300);
		seekBar.fadeIn(300);
		vid.currentTime = 0;
		$(window).click();
		goToView(1);
	},false);

	$("#seeker-line").on("click", function(e){
		var offset = $(this).offset();
		var left = (e.pageX - offset.left);
		var totalWidth = $("#seeker-line").width();
		var percentage = ( left / totalWidth );
		var vidTime = vid.duration * percentage;
		vid.currentTime = vidTime;
	});

	$('button[data-button="home"]').click(function(){
		vid.pause();
		playBtn.fadeIn(300);
		seekBar.fadeIn(300);
		vid.currentTime = 0;
	});

});

function controllValues(){


}

function setValue(appearance, value){
	$('.row_nupud[data-appearance="' + appearance + '"]').removeClass('selected_appearance');
	$('.row_nupud[data-appearance="' + appearance + '"][data-value="' + value + '"]').addClass('selected_appearance');

	if(appearance == 'length'){
		length = value;
	}
	if(appearance == 'weight'){
		weight = value;
	}
	if(appearance == 'hair_color'){
		hair_color = value;
	}
	if(appearance == 'eye_color'){
		eye_color = value;
	}
	if(appearance == 'education'){
		education = value;
	}
	if(appearance == 'smoking'){
		smoking = value;
	}
	if(appearance == 'alcohol'){
		alcohol = value;
	}
	if(appearance == 'sport'){
		sport = value;
	}
}

function loadResults(){
	$.ajax({
		type: "GET",
		url: "data/k12_new.csv",
		dataType: "text",
		success: function(res) {CsvData = res}
	});
}

function processData(allText) {
	//console.log('algus');
	count = null;
	var allTextLines = allText.split(/\r\n|\n/);
	var headers = allTextLines[0].split(',');
	for (var i=1; i<allTextLines.length; i++) {
		var data = allTextLines[i].split(',');
		//console.log(data[0]);
		if(	data[0] == gender_selection
				&& data[1] == age
				&& data[2] == length
				&& data[3] == weight
				&& data[4] == hair_color
				&& data[5] == eye_color
				&& data[6] == education
				&& data[7] == smoking
				&& data[8] == alcohol
				&& data[9] == sport

		){
			count = data[10];
			console.log(count);
		}
	}

}

function selectGender(gender){
	$('.choose_gender_images img').addClass('not_active');
	$('.choose_gender_images img[data-gender="' + gender + '"]').removeClass('not_active');
	gender_selection = gender;
	$('.view[data-view="2"] button[data-questionbtn="1"]').addClass('visible');
}

function goToView(viewNumber){
	$(".view").removeClass("show");
	$(".view:nth-of-type("+viewNumber+")").addClass("show");
	if(viewNumber != 1){
		jQuery(".view:nth-of-type("+viewNumber+")").css("background-color", "#252e38");
		jQuery(".home_btn").css("border", "1px solid #e8e9e4");
		jQuery(".home_btn").css("visibility", "");
		jQuery(".info_btn").css("visibility", "");
	}

	if(viewNumber == 2){
		jQuery("#playBtn").fadeOut(300);
		document.getElementById("video").play();
	}


}

// -- TRANSLATIONS
function getTranslations(lang) {
	jQuery('.languages div').removeClass('active');
	jQuery('.languages div[data-language="' + lang + '"]').addClass('active');
	jQuery('body').attr('data-language',lang);
	var json = getLangJSON(lang);
	jQuery.each(json[0], function (key, data) {
		jQuery('[data-translation="' + key + '"]').html(data).val(data);
		jQuery('[data-video="'+key+'"]').attr('data-src',data);
	});
}

function getLangJSON(lang){
	var json;
	if(lang == 'et'){
		json = translations.et;
	} else if(lang == 'en') {
		json = translations.en;
	} else if(lang == 'ru') {
		json = translations.ru;
	} else if(lang == 'fi') {
		json = translations.fi;
	}
	return json;
}

Number.prototype.map = function (in_min, in_max, out_min, out_max) {
	return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function getAge(x){
	var number = parseInt(round(x, 1)).map(0,100, 20,90);
	var min = Math.floor(number/5)*5;
	var max = min + 5;
	age = min + '-' + max;
	$('.min_age').html(min);
	$('.max_age').html(max);
}

function round(value, precision) {
	var multiplier = Math.pow(10, precision || 0);
	return Math.round(value * multiplier) / multiplier;
}

// Custom Checking Function..
function inRangeInclusive(start, end, value) {
	if (value <= end && value >= start)
		return value; // return given value
	return undefined;
}
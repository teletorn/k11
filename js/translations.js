var translations =
   {
      "et": [
         {
            "who_is_donor"    :  "Kes on geenidoonor?",
            "watch_video"     :  "VAATA VIDEOT!",
            "video_one"       :  "data/geenivaramu_ee.mp4",
         }
      ],
      "en": [
         {
            "who_is_donor"    :  "Who is a gene donor?",
            "watch_video"     :  "FIND OUT!",
            "video_one"       :  "data/geenivaramu_en.mp4",
         }
      ],
      "ru": [
         {
            "who_is_donor"    :  "Кто такой генный донор?",
            "watch_video"     :  "ВЫЯСНИТЕ!",
            "video_one"       :  "data/geenivaramu_ru.mp4",
         }
      // ],
      // "fi": [
      //    {
      //       "who_is_donor"    :  "Kuka on geenien luovuttaja?",
      //       "watch_video"     :  "Ota selvää!",
      //       "video_one"       :  "data/geenivaramu_fi.mp4",
      //    }
      ]
   }